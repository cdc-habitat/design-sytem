import React from "react";

interface IconProps {
  value: string;
}

export const Icon = ({ value = "primary", ...props }: IconProps) => {
  return <span className={`${value}`}></span>;
};
