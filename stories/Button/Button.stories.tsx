import type { Meta, StoryObj } from '@storybook/react';
import {Canvas, Story, Controls } from '@storybook/addon-docs';
import { Button } from './Button';

const meta: Meta<typeof Button>= {
  title: 'Components/Button',
  component: Button,
  tags: ['autodocs'],
  argTypes: {icon: {control: 'select', options: [undefined, 'icon-folder', 'icon-chevron-right']}},
  parameters: {
    docs: {
      page:() => 
      {
        return <>
        <Canvas>
          <Story />
        </Canvas>
        <Controls />
        </>
      }
    }
  }
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof Button>;

export const ExampleForDocs: Story = {
  args: {
    value: 'Button',
  },
};

export const Primary: Story = {
  render:()=> (
    <>
      <div>
        <Button value='Button' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' size='small' />
        <Button value='Button' size='medium' />
        <Button value='Button' size='large' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' size='small' icon="icon-chevron-right" hasIcon='left' />
        <Button value='Button' size='medium' icon="icon-chevron-right" hasIcon='left' />
        <Button value='Button' size='large' icon="icon-chevron-right" hasIcon='left' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' size='small' icon="icon-chevron-right" hasIcon='right'  />
        <Button value='Button' size='medium' icon="icon-chevron-right" hasIcon='right'  />
        <Button value='Button' size='large' icon="icon-chevron-right" hasIcon='right' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' size='small' icon="icon-chevron-right" iconOnly />
        <Button value='Button' size='medium' icon="icon-chevron-right" iconOnly />
        <Button value='Button' size='large' icon="icon-chevron-right" iconOnly />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start' , flexWrap: "wrap" }}>
        <Button value='Button' size='small' block  />
        <Button value='Button' size='medium' block  />
        <Button value='Button' size='large' block />
      </div>
    </>
  )
};

export const Secondary: Story = {
  render:()=> (
    <>
      <div>
        <Button value='Button' priority='secondary' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' priority='secondary' size='small' />
        <Button value='Button' priority='secondary' size='medium' />
        <Button value='Button' priority='secondary' size='large' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' priority='secondary' size='small'    icon="icon-chevron-right" hasIcon='left' />
        <Button value='Button' priority='secondary' size='medium'icon="icon-chevron-right" hasIcon='left' />
        <Button value='Button' priority='secondary' size='large' icon="icon-chevron-right" hasIcon='left' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button'priority='secondary' size='small' icon="icon-chevron-right"  hasIcon='right' />
        <Button value='Button'priority='secondary' size='medium' icon="icon-chevron-right" hasIcon='right' />
        <Button value='Button'priority='secondary' size='large' icon="icon-chevron-right" hasIcon='right' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button'priority='secondary' size='small' icon="icon-chevron-right" iconOnly />
        <Button value='Button'priority='secondary' size='medium' icon="icon-chevron-right" iconOnly />
        <Button value='Button'priority='secondary' size='large' icon="icon-chevron-right" iconOnly />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start' , flexWrap: "wrap" }}>
        <Button value='Button'priority='secondary' size='small' block />
        <Button value='Button'priority='secondary' size='medium' block />
        <Button value='Button'priority='secondary' size='large' block />
      </div>
    </>
  )
};

export const Tertiary: Story = {
  render:()=> (
    <>
      <div>
        <Button value='Button' priority='tertiary' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' priority='tertiary' size='small' />
        <Button value='Button' priority='tertiary' size='medium' />
        <Button value='Button' priority='tertiary' size='large' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' priority='tertiary' size='small'    icon="icon-chevron-right" hasIcon='left' />
        <Button value='Button' priority='tertiary' size='medium'icon="icon-chevron-right" hasIcon='left' />
        <Button value='Button' priority='tertiary' size='large' icon="icon-chevron-right" hasIcon='left' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' priority='tertiary' size='small' icon="icon-chevron-right"  hasIcon='right' />
        <Button value='Button' priority='tertiary' size='medium' icon="icon-chevron-right" hasIcon='right' />
        <Button value='Button' priority='tertiary' size='large' icon="icon-chevron-right" hasIcon='right' />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start'  }}>
        <Button value='Button' priority='tertiary' size='small' icon="icon-chevron-right" iconOnly />
        <Button value='Button' priority='tertiary' size='medium' icon="icon-chevron-right" iconOnly />
        <Button value='Button' priority='tertiary' size='large' icon="icon-chevron-right" iconOnly />
      </div>

      <div style={{ display: 'flex', gap: '1rem', marginTop: "2rem", alignItems: 'flex-start' , flexWrap: "wrap" }}>
        <Button value='Button' priority='tertiary' size='small' block />
        <Button value='Button' priority='tertiary' size='medium' block />
        <Button value='Button' priority='tertiary' size='large' block />
      </div>
    </>
  )
};
