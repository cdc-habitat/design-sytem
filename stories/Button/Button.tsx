import React from "react";

interface ButtonProps {
  /* default: primary*/
  priority?: "primary" | "secondary" | "tertiary";
  /* default: medium */
  size?: "small" | "medium" | "large";
  value: string;
  icon?: string;
  hasIcon?: undefined | "left" | "right";
  iconOnly?: boolean;
  block?: boolean;
  disabled?: boolean;
}

export const Button = ({
  priority = "primary",
  size = "medium",
  value,
  icon,
  hasIcon = "left",
  iconOnly,
  block,
  ...props
}: ButtonProps) => {
  return (
    <button
      type="button"
      className={[
        "btn",
        `btn--${size}`,
        `btn--${priority}`,
        `${block == true ? "btn--block" : ""}`,
        `${icon ?? ""}`,
        `${icon != undefined ? "btn--icon-"+hasIcon : ""}`,
        `${iconOnly == true ? "btn__icon-only" : ""}`
      ].join(" ")}
      {...props}
    >
      {value}
    </button>
  );
};
