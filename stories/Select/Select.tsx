import React from "react";

interface SelectProps {
  label?: string;
  /* default: medium */
  size?: "small" | "medium" | "large";
  helperText?: string;
  disabled?: boolean;
  error?: boolean;
}

export const Select = ({
  label = "Label",
  size = "medium",
  helperText = "Helper text",
  error,
  disabled,
  ...props
}: SelectProps) => {
  return (
    <div
      className={[
        "select-group",
        `${error == true ? "isInvalid" : ""}`,
        `${disabled == true ? "isDisabled" : ""}`,
      ].join(" ")}
    >
      {label != "" ? <label htmlFor="example">{label}</label> : ""}

        <select
          className={["select", `select--${size}`].join(" ")}
          id="example"
          name="example"
          disabled={disabled}
          {...props}
        >
          <option value="" selected hidden disabled>Selectionnez une option</option>
          <option value="option1">Option 1</option>
          <option value="option2">Option 2</option>
          <option value="option3">Option 3</option>
          <option value="option4">Option 4</option>
          <option value="option5">Option 5</option>
        </select>

      {helperText != "" ? (
        <span className="helper-text">{helperText}</span>
      ) : (
        ""
      )}
    </div>
  );
};
