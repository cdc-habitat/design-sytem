import type { Meta, StoryObj } from '@storybook/react';
import {Canvas, Story, Controls } from '@storybook/addon-docs';

import { Select } from './Select';

const meta: Meta<typeof Select>= {
  title: 'Components/Select',
  component: Select,
  tags: ['autodocs'],
  parameters: {
      docs: {
      page:() => 
      {
        return <>
          <Canvas>
            <Story />
          </Canvas>
          <Controls />
        </>
      }
    }
  }
} satisfies Meta<typeof Select>;

export default meta;
type Story = StoryObj<typeof Select>;

export const Default: Story = {
  args: {
    label: "Label",
  }
}