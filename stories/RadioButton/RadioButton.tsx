import React from "react";

interface RadioButtonProps {
  label?: string;
  disabled?: boolean;
}

export const RadioButton = ({
  label = "Label",
  disabled,
  ...props
}: RadioButtonProps) => {
  return (
    <div
      className={[
        "radio-group",
        `${disabled == true ? "isDisabled" : ""}`,
      ].join(" ")}
    >
      <label htmlFor="example">
        <input
          className="radio"
          type="radio"
          name="example"
          id="example"
          disabled={disabled}
        />
        {label ?? ""}
      </label>
    </div>
  );
};
