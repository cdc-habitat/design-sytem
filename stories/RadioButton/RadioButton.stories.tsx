import type { Meta, StoryObj } from '@storybook/react';
import {Canvas, Story, Controls } from '@storybook/addon-docs';

import { RadioButton } from './RadioButton';

const meta: Meta<typeof RadioButton>= {
  title: 'Components/RadioButton',
  component: RadioButton,
  tags: ['autodocs'],
  parameters: {
      docs: {
      page:() => 
      {
        return <>
          <Canvas>
            <Story />
          </Canvas>
          <Controls />
        </>
      }
    }
  }
} satisfies Meta<typeof RadioButton>;

export default meta;
type Story = StoryObj<typeof RadioButton>;

export const Default: Story = {
  args: {
    label: "Label",
  }
}