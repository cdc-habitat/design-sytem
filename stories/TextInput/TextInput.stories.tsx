import type { Meta, StoryObj } from '@storybook/react';
import {Canvas, Story, Controls } from '@storybook/addon-docs';

import { TextInput } from './TextInput';

const meta: Meta<typeof TextInput>= {
  title: 'Components/Text input',
  component: TextInput,
  tags: ['autodocs'],
  argTypes: {
    textarea: {
      control: 'boolean'
    },
    type: {
      if:{arg: 'textarea', neq: true}
    },
    icon: {
      control: 'select', 
      options: [undefined, 'icon-folder', 'icon-chevron-right'],
      if:{arg: 'textarea', neq: true}
    },
    hasIcon: {
      if:{arg: 'textarea', neq: true}
    },
    hasWordCount: {
      if:{arg:'textarea', eq: true}
    }
},
  parameters: {
    docs: {
      page:() => {
        return <>
          <Canvas>
            <Story />
          </Canvas>
          <Controls />
        </>
      }
    }
  }
} satisfies Meta<typeof TextInput>;

export default meta;
type Story = StoryObj<typeof TextInput>;

export const ExampleForDocs: Story = {
  args: {
    label: "Label",
  }
}