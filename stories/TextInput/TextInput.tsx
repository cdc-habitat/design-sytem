import React from "react";

interface TextInputProps {
  label?: string;
  /* default: medium */
  size?: "small" | "medium" | "large";
  textarea?: boolean;
  /* default: text */
  type?: "text" | "email" | "tel" | "date" | "password" | "number";
  value?: string;
  icon?: string;
  hasIcon?: undefined | "left" | "right";
  placeholder?: string;
  helperText?: string;
  hasWordCount?: boolean;
  disabled?: boolean;
  error?: boolean;
}

export const TextInput = ({
  label = "Label",
  textarea,
  type,
  size = "medium",
  value,
  icon,
  hasIcon = "left",
  placeholder = "Placeholder",
  helperText = "Helper text",
  hasWordCount,
  error,
  disabled,
  ...props
}: TextInputProps) => {
  if (textarea) {
    return (
      <div
        className={[
          "input-group",
          `${error == true ? "isInvalid" : ""}`,
          `${disabled == true ? "isDisabled" : ""}`,
        ].join(" ")}
      >
        {label != "" ? <label htmlFor="example">{label}</label> : ""}

        <textarea
          className={["input", `input--${size}`].join(" ")}
          id="example"
          name="example"
          placeholder={placeholder}
          disabled={disabled}
          {...props}
        >
          {value}
        </textarea>

        <div className="input-bottom-infos">
          {helperText != "" ? (
            <span className="helper-text">{helperText}</span>
          ) : (
            ""
          )}
          {hasWordCount == true ? (
            <span className="word-count">0/100</span>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }

  if (icon) {
    return (
      <div
        className={[
          "input-group",
          `input-group--${size}`,
          `${error == true ? "isInvalid" : ""}`,
          `${disabled == true ? "isDisabled" : ""}`,
        ].join(" ")}
      >
        {label != "" ? <label htmlFor="example">{label}</label> : ""}

        <div
          className={["input-wrap", `${icon}`, `input--icon-${hasIcon}`].join(
            " "
          )}
        >
          <input
            type={type}
            className={["input", `input--${size}`].join(" ")}
            id="example"
            name="example"
            placeholder={placeholder}
            value={value}
            disabled={disabled}
            {...props}
          />
        </div>

        <div className="input-bottom-infos">
          {helperText != "" ? (
            <span className="helper-text">{helperText}</span>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }

  return (
    <div
      className={[
        "input-group",
        `${error == true ? "isInvalid" : ""}`,
        `${disabled == true ? "isDisabled" : ""}`,
      ].join(" ")}
    >
      {label != "" ? <label htmlFor="example">{label}</label> : ""}
      <input
        type={type}
        className={["input", `input--${size}`].join(" ")}
        id="example"
        name="example"
        placeholder={placeholder}
        value={value}
        disabled={disabled}
        {...props}
      />

      <div className="input-bottom-infos">
        {helperText != "" ? (
          <span className="helper-text">{helperText}</span>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};
