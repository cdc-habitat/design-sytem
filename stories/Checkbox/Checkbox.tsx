import React from "react";

interface CheckboxProps {
  label?: string;
  disabled?: boolean;
}

export const Checkbox = ({
  label = "Label",
  disabled,
  ...props
}: CheckboxProps) => {
  return (
    <div
      className={[
        "checkbox-group",
        `${disabled == true ? "isDisabled" : ""}`,
      ].join(" ")}
    >
      <label htmlFor="example">
        <input
          className="checkbox"
          type="checkbox"
          name="example"
          id="example"
          disabled={disabled}
        />
        {label ?? ""}
      </label>
    </div>
  );
};
