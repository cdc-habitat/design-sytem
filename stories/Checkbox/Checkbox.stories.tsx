import type { Meta, StoryObj } from '@storybook/react';
import {Canvas, Story, Controls } from '@storybook/addon-docs';

import { Checkbox } from './Checkbox';

const meta: Meta<typeof Checkbox>= {
  title: 'Components/Checkbox',
  component: Checkbox,
  tags: ['autodocs'],
  parameters: {
      docs: {
      page:() => 
      {
        return <>
          <Canvas>
            <Story />
          </Canvas>
          <Controls />
        </>
      }
    }
  }
} satisfies Meta<typeof Checkbox>;

export default meta;
type Story = StoryObj<typeof Checkbox>;

export const Default: Story = {
  args: {
    label: "Label",
  }
}