import React from "react";

interface ToggleProps {
  label?: string;
  disabled?: boolean;
}

export const Toggle = ({
  label = "Label",
  disabled,
  ...props
}: ToggleProps) => {
  return (
    <div className={["toggle-group", `${disabled == true ? "isDisabled" : ""}`].join(
      " "
    )}>
      <label
        className="toggle"
        htmlFor="example"
      >
        <input type="checkbox" id="example" disabled={disabled} />
        <span aria-hidden="true" hidden></span>
        {label}
      </label>
    </div>
  );
};
