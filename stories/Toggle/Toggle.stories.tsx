import type { Meta, StoryObj } from '@storybook/react';
import {Canvas, Story, Controls } from '@storybook/addon-docs';

import { Toggle } from './Toggle';

const meta: Meta<typeof Toggle>= {
  title: 'Components/Toggle',
  component: Toggle,
  tags: ['autodocs'],
  parameters: {
      docs: {
      page:() => 
      {
        return <>
          <Canvas>
            <Story />
          </Canvas>
          <Controls />
        </>
      }
    }
  }
} satisfies Meta<typeof Toggle>;

export default meta;
type Story = StoryObj<typeof Toggle>;

export const Default: Story = {
  args: {
    label: "Label",
  }
}