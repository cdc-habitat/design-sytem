import type { Preview } from "@storybook/react";
import "../assets/styles/design-system.scss";

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    options: {
      storySort: {
        order: ["Styles", "Components"],
      },
    },
  },
};

export default preview;
