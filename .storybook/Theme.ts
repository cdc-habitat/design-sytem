import { create } from '@storybook/theming/create';

export default create({
  base: 'light',
  brandTitle: 'CDC Habitat | Design system',
  brandImage: '/assets/images/logoCdcHabitat.svg',

  colorPrimary: '#E30613',

   // Text colors
   textColor: '#000000',
   textInverseColor: '#ffffff',

   // UI
   appBg: '#ffffff',
   appContentBg: '#ffffff',
   appBorderColor: '#585C6D',
   appBorderRadius: 0,

  // Toolbar default and active colors
  barTextColor: '#000',
  barSelectedColor: '#585C6D',
  barBg: '#ffffff',
});
